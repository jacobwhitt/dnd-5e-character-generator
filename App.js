import React from "react";
import { Alert, Dimensions, StatusBar, StyleSheet } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
const Stack = createStackNavigator();

import { Backgrounds } from "./components/backgrounds";
import { Classes } from "./components/classes";
import { Races } from "./components/races";
import RNG from "./components/RNG";
const { width, height } = Dimensions.get("window");

import NavigationScreen from "./views/Navigation";

const defaultState = {
  user: {
    name: null,
    // characterPhoto: null,
    selectedRace: {},
    selectedClass: {},
    selectedBackground: {},
    statsModded: false,
  },
  backgroundNames: [],
  character: [],
  classNames: [],
  loggedIn: false,
  name: "",
  raceNames: [],
};

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {
        name: null,
        // characterPhoto: null,
        selectedRace: {},
        selectedClass: {},
        selectedBackground: {},
        statsModded: false,
      },
      backgroundNames: [],
      character: [],
      classNames: [],
      name: "",
      loggedIn: false,
      raceNames: [],
    };
  }

  componentDidMount() {
    console.log("mounting App.js");
    this.readStorage();

    let tempArr = [];

    Races.forEach((item) => {
      tempArr.push({ name: item.name, id: RNG() });
      this.setState({ raceNames: [...tempArr] });
    });
    tempArr = [];

    Classes.forEach((item) => {
      tempArr.push({ name: item.name, id: RNG() });
      this.setState({ classNames: [...tempArr] });
    });
    tempArr = [];

    Backgrounds.forEach((item) => {
      tempArr.push({ name: item.name, id: RNG() });
      this.setState({ backgroundNames: [...tempArr] });
    });
    tempArr = [];
  }

  login = async () => {
    const _id = Math.random().toString(36).substring(7);
    const newUser = { id: _id, name: this.state.user.name };
    console.log("New user logged in: ", newUser);

    await AsyncStorage.setItem("user", JSON.stringify(newUser));
    this.setState({ user: newUser, name: "", loggedIn: true });
  };

  logout = () => {
    this.setState(defaultState);
    setTimeout(() => {
      console.log("state reset to default: ", this.state);
    }, 1000);
    AsyncStorage.clear();
    this.readStorage();
  };

  modifyStats = async () => {
    try {
      let moddedStats = { ...this.state.user, statsModded: true };
      this.setState({ user: moddedStats });
      await AsyncStorage.setItem("user", JSON.stringify(moddedStats));

      setTimeout(() => {
        this.readStorage();
      }, 500);
    } catch (e) {
      console.log("An error occured in modifyStats: ", e);
    }
  };

  readStorage = async () => {
    console.log("readStorage");
    const user = await AsyncStorage.getItem("user");
    if (user) {
      console.log("readStorage found user in storage: ", JSON.parse(user));
      this.setState({ user: JSON.parse(user), loggedIn: true });
    }
    if (!user) {
      console.log("User is logged out");
    }
  };

  setClass = async (index) => {
    try {
      let classInfo = Classes[index];

      let newClass = {
        ...this.state.user,
        selectedClass: classInfo,
        statsModded: false,
      };
      this.setState({ user: newClass });
      await AsyncStorage.setItem("user", JSON.stringify(newClass));

      setTimeout(() => {
        this.readStorage();
      }, 500);
    } catch (e) {
      console.log("An error occured in setClass: ", e);
    }
  };

  setBackground = async (index) => {
    try {
      let backgroundInfo = Backgrounds[index];

      let newBackground = {
        ...this.state.user,
        selectedBackground: backgroundInfo,
      };

      this.setState({ user: newBackground });
      await AsyncStorage.setItem("user", JSON.stringify(newBackground));

      setTimeout(() => {
        this.readStorage();
      }, 500);
    } catch (e) {
      console.log("An error occured in setBackground: ", e);
    }
  };

  setRace = async (index) => {
    try {
      let raceInfo = Races[index];

      let newRace = {
        ...this.state.user,
        selectedRace: raceInfo,
        statsModded: false,
      };

      this.setState({ user: newRace });
      await AsyncStorage.setItem("user", JSON.stringify(newRace));

      setTimeout(() => {
        this.readStorage();
      }, 500);
    } catch (e) {
      console.log("An error occured in setRace: ", e);
    }
  };

  setName = (text) => {
    if (/^[a-zA-Z0-9\s]+$/.test(text) || text === "") {
      // console.log("setName: ", text);
      let capitalized = text.substring(0, 1).toUpperCase() + text.substring(1);
      this.setState({ user: { ...this.state.user, name: capitalized } });
    } else {
      Alert.alert("Please only use alphanumeric characters and spaces");
    }
  };

  render() {
    StatusBar.setBarStyle("dark-content", true);

    return (
      <NavigationContainer style={styles.container}>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
            gestureEnabled: false,
          }}
        >
          <Stack.Screen name="Navigation">
            {(props) => (
              <NavigationScreen
                {...props}
                login={this.login}
                logout={this.logout}
                loggedIn={this.state.loggedIn}
                modifyStats={this.modifyStats}
                openCharacterSheet={this.openCharacterSheet}
                raceNames={this.state.raceNames}
                classNames={this.state.classNames}
                backgroundNames={this.state.backgroundNames}
                readStorage={this.readStorage}
                setRace={this.setRace}
                setClass={this.setClass}
                setBackground={this.setBackground}
                setName={this.setName}
                user={this.state.user}
              />
            )}
          </Stack.Screen>
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: width,
    height: height,
    alignItems: "center",
    justifyContent: "center",
  },
});
