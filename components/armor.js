export const Weapons = [
  { name: "Leather Armor", ac: "11" },
  { name: "Studded Leather Armor", ac: "12" },
  { name: "Scale Mail", ac: "14" },
  { name: "Chain Mail", ac: "16" },
];
