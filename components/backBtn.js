import React from "react";
import { StyleSheet, Text } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { TouchableOpacity } from "react-native-gesture-handler";
import Icon from "react-native-vector-icons/FontAwesome";

const BackBtn = ({ text, onPress, color }) => {
  const navigation = useNavigation();
  const backArrow = <Icon name="arrow-left" size={10} color="#fff" />;

  return (
    <TouchableOpacity
      style={[
        styles.button,
        { width: text.length > 6 ? 100 : 70, backgroundColor: color },
      ]}
      onPress={() => {
        navigation.navigate(onPress);
      }}
    >
      {backArrow}
      <Text style={styles.btnText}>{text}</Text>
    </TouchableOpacity>
  );
};

export default BackBtn;

const styles = StyleSheet.create({
  button: {
    // position: "absolute",
    top: 20,
    left: 20,
    flexDirection: "row",
    width: 70,
    height: 35,
    marginTop: 20,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "space-evenly",
    shadowColor: "rgba(0,0,0, .3)", // IOS
    shadowOffset: { height: 3, width: 3 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    elevation: 2, // Android
    zIndex: 2,
  },
  btnText: {
    color: "#fff",
    fontSize: 18,
  },
});
