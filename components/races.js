export const Races = [
  {
    name: "Dragonborn",
    abilitymods: { STR: 2, CHA: 1 },
    proficiencies: [],
    languages: ["Common", "Draconic"],
    size: "Medium",
    speed: 30,
    vision: ["Normal"],
    spells: [],
    features: [
      {
        name: "Breath Weapon",
        text: "You can use your action to exhale destructive energy. Your draconic ancestry determines the size, shape, and damage type of the exhalation. When you use your breath weapon, each creature in the area of the exhalation must make a saving throw, the type of which is determined by your draconic ancestry. The DC for this saving throw equals 8 + your Constitution modifier + your proficiency bonus. A creature takes 2d6 damage on a failed save, and half as much damage on a successful one. The damage increases to 3d6 at 6th level, 4d6 at 11th level, and 5d6 at 16th level. After you use your breath weapon, you can't use it again until you complete a short or long rest.",
      },
      { name: "Damage Resistance", text: "You have resistance to the damage type associated with your draconic ancestry." },
      {
        name: "Draconic Ancestry",
        text: "You have draconic ancestry. Choose one type of dragon from the Draconic Ancestry table. Your breath weapon and damage resistance are determined by the dragon type, as shown in the table.",
        dragon: {
          color: ["Black", "Blue", "Brass", "Bronze", "Copper", "Gold", "Green", "Red", "Silver", "White"],
          damage: ["Acid", "Lightning", "Fire", "Lightning", "Acid", "Fire", "Poison", "Fire", "Cold", "Cold"],
          breath: ["5 by 30ft. line, DEX save", "5 by 30ft. line, DEX save", "5 by 30ft. line, DEX save", "5 by 30ft. line, DEX save", "5 by 30ft. line, DEX save", "15ft. cone, DEX save", "15ft. cone, CON save", "15ft. cone, DEX save", "15ft. cone, CON save", "15ft. cone, CON save"],
        },
      },
    ],
  },
  {
    name: "Dwarf",
    abilitymods: { STR: 2, CON: 2 },
    proficiencies: ["battleaxe", "handaxe", "light hammer", "warhammer", "light armor", "medium armor"],
    languages: ["Common", "Dwarvish"],
    size: "Medium",
    speed: 25,
    vision: ["Normal", "Darkvision"],
    spells: [],
    features: [
      { name: "Dwarven Speed", text: "Your speed is not reduced by wearing heavy armor." },
      { name: "Dwarven Resilience", text: "You have advantage on saving throws against poison, and you have resistance against poison damage." },
      { name: "Dwarven Combat Training", text: "You have proficiency with the battleaxe, handaxe, light hammer, and warhammer." },
      { name: "Drawven Armor Training", text: "You have proficiency with light and medium armor." },
      { name: "Tool Proficiency", text: "You gain proficiency with the artisan's tools of your choice: Smith's tools, brewer's supplies, or mason's tools." },
      { name: "Stonecunning", text: "Whenever you make an Intelligence (History) check related to the origin of stonework, you are considered proficient in the History skill and add double your proficiency bonus to the check, instead of your normal proficiency bonus." },
    ],
  },
  {
    name: "Elf",
    abilitymods: { DEX: 2, WIS: 1 },
    proficiencies: ["Perception", "Longsword", "Shortsword", "Shortbow", "Longbow"],
    languages: ["Common", "Elvish"],
    size: "Medium",
    speed: 35,
    vision: ["Normal", "Darkvision"],
    spells: [],
    features: [
      { name: "Fey Ancestry", text: "You have advantage on saving throws against being charmed, and magic can't put you to sleep." },
      { name: "Elf Weapon Training", text: "You have proficiency with the longsword, shortsword, shortbow, and longbow." },
      {
        name: "Trance",
        text: "Elves don't need to sleep. Instead, they meditate deeply, remaining semiconscious, for 4 hours a day. (The Common word for such meditation is 'trance.') While meditating, you can dream after a fashion; such dreams are actually mental exercises that have become reflexive through years of practice. After resting in this way, you gain the same benefit that a human does from 8 hours of sleep. If you meditate during a long rest, you finish the rest after only 4 hours. You otherwise obey all the rules for a long rest; only the duration is changed.",
      },
      { name: "Fleet of Foot", text: "Your base walking speed increases to 35 feet." },
      { name: "Mask of the Wild", text: "You can attempt to hide even when you are only lightly obscured by foliage, heavy rain, falling snow, mist, and other natural phenomena." },
    ],
  },
  {
    name: "Gnome",
    abilitymods: { INT: 2, CON: 1 },
    proficiencies: [],
    languages: ["Common", "Gnomish"],
    size: "Small",
    speed: 30,
    vision: ["Normal"],
    spells: [],
    features: [
      { name: "Gnome Cunning", text: "You have advantage on all Intelligence, Wisdom, and Charisma saving throws against magic." },
      { name: "Artificer's Lore", text: "Whenever you make an Intelligence (History) check related to magic items, alchemical objects, or technological devices, you can add twice your proficiency bonus, instead of any proficiency bonus you normally apply." },
      {
        name: "Tinker",
        text: "You have proficiency with artisan's tools (tinker's tools). Using those tools, you can spend 1 hour and 10 gp worth of materials to construct a Tiny clockwork device (AC 5, 1 hp). The device ceases to function after 24 hours (unless you spend 1 hour repairing it to keep the device functioning), or when you use your action to dismantle it; at that time, you can reclaim the materials used to create it. You can have up to three such devices active at a time. When you create a device, choose one of the following options: Clockwork Toy - This toy is a clockwork animal, monster, or person, such as a frog, mouse, bird, dragon, or soldier. When placed on the ground, the toy moves 5 feet across the ground on each of your turns in a random direction. It makes noises as appropriate to the creature it represents. Fire Starter - The device produces a miniature flame, which you can use to light a candle, torch, or campfire. Using the device requires your action. Music Box - When opened, this music box plays a single song at a moderate volume. The box stops playing when it reaches the song's end or when it is closed.",
      },
    ],
  },
  {
    name: "Half-elf",
    abilitymods: { CHA: 2 },
    proficiencies: [],
    languages: ["Common", "Elvish"],
    size: "Medium",
    speed: 30,
    vision: ["Normal", "Darkvision"],
    spells: [],
    features: [
      { name: "Fey Ancestry", text: "You have advantage on saving throws against being charmed, and magic can't put you to sleep." },
      { name: "Skill Versatility", text: "You gain proficiency in two skills of your choice." },
    ],
  },
  {
    name: "Half-orc",
    abilitymods: { STR: 2, CON: 1 },
    proficiencies: ["Intimidation"],
    languages: ["Common", "Orc"],
    size: "Medium",
    speed: 30,
    vision: ["Normal", "Darkvision"],
    spells: [],
    features: [
      { name: "Menacing", text: "You gain proficiency in the Intimidation skill." },
      { name: "Relentless Endurance", text: "When you are reduced to 0 hit points but not killed outright, you can drop to 1 hit point instead. You can't use this feature again until you finish a long rest." },
      { name: "Savage Attacks", text: "When you score a critical hit with a melee weapon attack, you can roll one of the weapon's damage dice one additional time and add it to the extra damage of the critical hit." },
    ],
  },
  {
    name: "Halfling",
    abilitymods: { DEX: 2, CHA: 1 },
    proficiencies: [],
    languages: ["Common", "Halfling"],
    size: "Small",
    speed: 25,
    vision: ["Normal"],
    spells: [],
    features: [
      { name: "Lucky", text: "When you roll a 1 on an attack roll, ability check, or saving throw, you can reroll the die and must use the new roll." },
      { name: "Brave", text: "You have advantage on saving throws against being frightened." },
      { name: "Halfling Nimbleness", text: "You can move through the space of any creature that is of a size larger than yours." },
      { name: "Naturally Stealthy", text: "You can attempt to hide even when you are obscured only by a creature that is at least one size larger than you." },
    ],
  },
  {
    name: "Human",
    abilitymods: { STR: 1, DEX: 1, CON: 1, INT: 1, WIS: 1, CHA: 1 },
    proficiencies: [],
    languages: ["Common"],
    size: "Medium",
    speed: 30,
    vision: ["Normal"],
    spells: [],
    features: [
      {
        name: "Languages",
        text: "You can speak, read, and write Common and one extra language of your choice. Humans typically learn the languages of other peoples they deal with, including obscure dialects. They are fond of sprinkling their speech with words borrowed from other tongues: Orc curses, Elvish musical expressions, Dwarvish military phrases, and so on.",
      },
    ],
  },
  {
    name: "Tiefling",
    abilitymods: { CHA: 2, INT: 1 },
    proficiencies: [],
    languages: ["Common", "Infernal"],
    size: "Medium",
    speed: 30,
    vision: ["Normal", "Darkvision"],
    spells: ["Thaumaturgy"],
    features: [
      { name: "Hellish Resistance", text: "You have resistance to fire damage." },
      {
        name: "Infernal Legacy",
        text: "You know the thaumaturgy cantrip. Once you reach 3rd level, you can cast the hellish rebuke spell as a 2nd-level spell; you must finish a long rest in order to cast the spell again using this trait. Once you reach 5th level, you can also cast the darkness spell; you must finish a long rest in order to cast the spell again using this trait. Charisma is your spellcasting ability for these spells.",
      },
    ],
  },
];
