import React, { useCallback, useEffect, useState } from "react";
import { useFocusEffect } from "@react-navigation/native";
import {
  ActivityIndicator,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Share,
  StatusBar,
} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import Icon from "react-native-vector-icons/FontAwesome";
import { Weapons } from "../components/weapons";

const CharacterSheet = (props) => {
  const [loading, setLoading] = useState(false);
  // const [photo, setPhoto] = useState("");

  let myRace = props.user.selectedRace;
  let myClass = props.user.selectedClass;
  let myBackground = props.user.selectedBackground;
  const toolSorter = [];

  //! ARRAYS STORING HTML RENDERED IN LOOPS
  let characterStats = {
    abilityScores: [],
    armor_proficiencies: [],
    equipment: [],
    languageList: [],
    racialFeatures: [],
    saving_throws: [],
    skill_proficiencies: [],
    tool_proficiencies: [],
    visionTypes: [],
    weapon_proficiencies: [],
    weapon_stats: [],
  };

  useEffect(() => {
    if (props.loggedIn) {
      console.log("————————  Character Sheet UseEffect ————————");
      setLoading(true);

      setTimeout(() => {
        setLoading(false);
      }, 1000);
    }
  }, []);

  useFocusEffect(
    // MUST WRAP useFocusEffect's CONTENT IN useCallback OR IT RUNS EVERY RENDER
    useCallback(() => {
      if (props.loggedIn) {
        calcAbilities();
        setWeaponStats();

        // setPhoto(props.photoURI);
      }
    }, [])
  );

  //! APPLY ABILITY MODIFIERS TO STAT BLOCK
  const calcAbilities = () => {
    characterStats.abilityScores = myClass.ability_scores;

    // MODIFIERS - EACH RACE HAS 2
    let mods = Object.keys(myRace.abilitymods);
    // PREMADE STATS BLOCK FOR EACH CLASS
    let scores = Object.keys(myClass.ability_scores);

    if (props.user.statsModded === false) {
      // console.log("Modifying stats!");

      mods.forEach((mod) => {
        // console.log("entering mod loop", mod);
        scores.forEach((score) => {
          // console.log("entering score loop", score);
          if (mod === score) {
            // console.log(
            //   `${mod} is being modded from:`,
            //   myClass.ability_scores[mod],
            //   "to: ",
            //   myClass.ability_scores[mod] + myRace.abilitymods[mod]
            // );

            characterStats.abilityScores[mod] =
              myClass.ability_scores[mod] + myRace.abilitymods[mod];

            // console.log(`new ${mod} score: `, myClass.ability_scores[mod]);
          }
        });
      });
      // console.log("running modifyStats via props");
      props.modifyStats();
    }
    // console.log("end of calcAbilities");
  };

  //! SHARE ALL HTML TEXT AS PLAIN TEXT TO ANOTHER APP
  const onShare = async () => {
    try {
      const result = await Share.share({
        message: `Character Name: ${props.user.name}\n\n Race: ${
          myRace.name
        }\n Class: ${myClass.name}\n Background: ${myBackground.name}\n HP: ${
          myClass.hit_points
        }\n Hit Dice: ${myClass.hit_dice}\n Languages: ${
          myRace.languages
        }\n Size: ${myRace.size}\n Speed: ${myRace.speed}\n Vision: ${
          myRace.vision
        }\n Gold: ${myBackground.gold}\n Saving Throws: ${
          myClass.saving_throws
        }\n Skill proficiencies: ${myClass.skill_proficiencies}\n Cantrips: ${
          myClass.cantrips
        }\n Spells: ${
          myRace.spells.length > 0 ? myRace.spells : myClass.spells
        }\n\n STR: ${myClass.ability_scores.STR}\n DEX: ${
          myClass.ability_scores.DEX
        }\n CON: ${myClass.ability_scores.CON}\n INT: ${
          myClass.ability_scores.INT
        }\n WIS: ${myClass.ability_scores.WIS}\n CHA: ${
          myClass.ability_scores.CHA
        }\n\nArmor proficiencies: ${
          myClass.armor_proficiencies
        }\n\nWeapon proficiencies: ${
          myClass.weapon_proficiencies
        }\n\nTool proficiencies: ${
          myClass.tool_proficiencies + ", " + myBackground.tool_proficiencies
        }\n\nStarting Equipment: ${
          myClass.starting_equipment
        }\n\nRacial Features: ${myRace.features.map(
          (item) => `\n\n${item.name}: ${item.text}`
        )}`,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  //! LOOPING FN's TO CREATE HTML IN ARRAYS ABOVE

  // CLASS SAVING THROWS
  characterStats.saving_throws.push(
    myClass.saving_throws.map((item, i) => (
      <Text style={styles.text} key={i}>
        {i + 1 === myClass.saving_throws.length ? item : item + ", "}
      </Text>
    ))
  );

  // CLASS ARMOR PROFICIENCIES
  characterStats.armor_proficiencies.push(
    myClass.armor_proficiencies.map((item, i) => (
      <Text style={styles.text} key={i}>
        {i + 1 === myClass.armor_proficiencies.length ? item : item + ", "}
      </Text>
    ))
  );

  // CLASS WEAPON PROFICIENCIES
  characterStats.weapon_proficiencies.push(
    myClass.weapon_proficiencies.map((item, i) => (
      <Text style={styles.text} key={i}>
        {i + 1 === myClass.weapon_proficiencies.length ? item : item + ", "}
      </Text>
    ))
  );

  // CLASS STARTING EQUIPMENT
  characterStats.equipment.push(
    myClass.starting_equipment.map((item, i) => (
      <Text style={styles.text} key={i}>
        {i + 1 === myClass.starting_equipment.length ? item : item + ", "}
      </Text>
    ))
  );

  //TODO WEAPON DETAILS FOR COMBAT --------------------------------------
  const setWeaponStats = () => {
    const tempWeaponsArr = [];

    // SET MARTIAL WEAPONS
    myClass.starting_equipment.forEach((item) => {
      Weapons.martial.forEach((martialWeapon) => {
        if (item === martialWeapon.name) {
          tempWeaponsArr.push(martialWeapon);
          console.log("Martial weapon info added to arr: ", martialWeapon);
        }
      });
    });

    // SET SIMPLE WEAPONS
    myClass.starting_equipment.forEach((item) => {
      Weapons.simple.forEach((simpleWeapon) => {
        if (item === simpleWeapon.name) {
          tempWeaponsArr.push(simpleWeapon);
          console.log("Simple weapon info added to arr: ", simpleWeapon);
        }
      });
    });
    console.log("———————— tempWepArr: ————————\n", tempWeaponsArr);

    // SET ALL WEAPONS INTO WEAPONS_STATS ARRAY
    const weaponsListArr = tempWeaponsArr.map((item, i) => {
      return (
        <Text key={i}>
          <Text style={styles.text}>
            {item.name}: {item.damage} ({item.props}
            {item.range ? ` range: ${item.range}) \n` : ") \n"}
          </Text>
        </Text>
      );
    });

    return weaponsListArr;
  };

  //TODO WEAPON DETAILS FOR COMBAT --------------------------------------

  // RACIAL LANGUAGES
  characterStats.languageList.push(
    myRace.languages.map((item, i) => (
      <Text style={styles.text} key={i}>
        {i + 1 === myRace.languages.length ? item : item + ", "}
      </Text>
    ))
  );

  // CLASS SKILL PROFICIENCIES
  characterStats.skill_proficiencies.push(
    myClass.skill_proficiencies.map((item, i) => (
      <Text style={styles.text} key={i}>
        {i + 1 === myClass.skill_proficiencies.length ? item : item + ", "}
      </Text>
    ))
  );

  //! CONSOLIDATING ALL TOOL PROFICIENCIES BEFORE SETTING THE FINAL LIST
  myClass.tool_proficiencies.map((item) => {
    toolSorter.push(item);
  });
  myBackground.tool_proficiencies.map((item) => {
    if (!toolSorter.includes(item)) {
      toolSorter.push(item);
    }
  });

  //? SET FINAL TOOL PROFICIENCIES LIST TO RENDER
  characterStats.tool_proficiencies.push(
    toolSorter.map((item, i) => (
      <Text style={styles.text} key={i}>
        {i + 1 === toolSorter.length ? item : item + ", "}
      </Text>
    ))
  );

  // RACIAL VISION TYPES
  characterStats.visionTypes.push(
    myRace.vision.map((item, i) => (
      <Text style={styles.text} key={i}>
        {i + 1 === myRace.vision.length ? item : item + ", "}
      </Text>
    ))
  );

  // RACIAL FEATURES
  characterStats.racialFeatures.push(
    myRace.features.map((item, i) => (
      <View key={i}>
        <Text style={styles.infoText}>Racial Features:</Text>
        <Text style={styles.textContainer}>
          <Text style={styles.infoText}>{item.name}: </Text>
          <Text style={styles.text}>{item.text}</Text>
        </Text>
      </View>
    ))
  );

  return (
    <SafeAreaView style={styles.container} contentContainerStyle={styles.ccs}>
      {loading ? (
        <View style={styles.loadingContainer}>
          <Text style={styles.loadingText}>SUMMONING YOUR INFORMATION</Text>
          <ActivityIndicator
            style={{ margin: 10 }}
            color="#9d0208"
            size="large"
          />
        </View>
      ) : (
        <ScrollView>
          <View style={styles.main}>
            {props.user.name.length > 15 ? (
              <Text style={[styles.title, { fontSize: 24 }]}>
                {props.user.name}'s Info:
              </Text>
            ) : (
              <Text style={styles.title}>{props.user.name}'s Info:</Text>
            )}

            {/*//! CHARACTER IMAGE */}
            {/* {photo ? (
                <View style={styles.imageContainer}>
                  <Image style={styles.image} source={{ uri: photo }} />
                </View>
              ) : null} */}

            {/*//! CHARACTER INFO */}
            <View style={styles.bottom}>
              {/*//! BASIC INFO */}
              <Text style={styles.textContainer}>
                <Text style={styles.sectionTitle}>Basic Info</Text>
              </Text>
              <Text style={styles.textContainer}>
                <Text style={styles.infoText}>Race:</Text>{" "}
                <Text style={styles.text}>{myRace.name}</Text>
              </Text>
              <Text style={styles.textContainer}>
                <Text style={styles.infoText}>Class:</Text>{" "}
                <Text style={styles.text}>{myClass.name}</Text>
              </Text>
              <Text style={styles.textContainer}>
                <Text style={styles.infoText}>Background:</Text>{" "}
                <Text style={styles.text}>{myBackground.name}</Text>
              </Text>
              <Text style={styles.textContainer}>
                <Text style={styles.infoText}>Hit Points:</Text>{" "}
                <Text style={styles.text}>{myClass.hit_points}</Text>
              </Text>
              <Text style={styles.textContainer}>
                <Text style={styles.infoText}>Hit Dice:</Text>{" "}
                <Text style={styles.text}>{myClass.hit_dice}</Text>
              </Text>
              <Text style={styles.textContainer}>
                <Text style={styles.infoText}>Languages:</Text>{" "}
                <Text style={styles.text}>{characterStats.languageList}</Text>
              </Text>
              <Text style={styles.textContainer}>
                <Text style={styles.infoText}>Size:</Text>{" "}
                <Text style={styles.text}>{myRace.size}</Text>
              </Text>
              <Text style={styles.textContainer}>
                <Text style={styles.infoText}>Speed:</Text>{" "}
                <Text style={styles.text}>{myRace.speed}</Text>
              </Text>
              <Text style={styles.textContainer}>
                <Text style={styles.infoText}>Vision:</Text>{" "}
                <Text style={styles.text}>{characterStats.visionTypes}</Text>
              </Text>
              <Text style={styles.textContainer}>
                <Text style={styles.infoText}>Gold:</Text>{" "}
                <Text style={styles.text}>{myBackground.gold}</Text>
              </Text>

              {/* //! Weapons Stats  */}
              <Text style={styles.infoText}>Weapons:</Text>
              <Text style={styles.text}>{setWeaponStats()}</Text>

              {/* DIVIDER */}
              <View style={styles.divider} />

              {/*//! STATS */}
              <Text style={styles.textContainer}>
                <Text style={styles.sectionTitle}>Ability Scores</Text>
              </Text>
              <Text style={styles.textContainer}>
                <Text style={styles.infoText}>STR:</Text>{" "}
                <Text style={styles.text}>{myClass.ability_scores.STR}</Text>
              </Text>
              <Text style={styles.textContainer}>
                <Text style={styles.infoText}>DEX:</Text>{" "}
                <Text style={styles.text}>{myClass.ability_scores.DEX}</Text>
              </Text>
              <Text style={styles.textContainer}>
                <Text style={styles.infoText}>CON:</Text>{" "}
                <Text style={styles.text}>{myClass.ability_scores.CON}</Text>
              </Text>
              <Text style={styles.textContainer}>
                <Text style={styles.infoText}>INT:</Text>{" "}
                <Text style={styles.text}>{myClass.ability_scores.INT}</Text>
              </Text>
              <Text style={styles.textContainer}>
                <Text style={styles.infoText}>WIS:</Text>{" "}
                <Text style={styles.text}>{myClass.ability_scores.WIS}</Text>
              </Text>
              <Text style={styles.textContainer}>
                <Text style={styles.infoText}>CHA:</Text>{" "}
                <Text style={styles.text}>{myClass.ability_scores.CHA}</Text>
              </Text>

              {/* DIVIDER */}
              <View style={styles.divider} />

              {/* //! Class & Race Details  */}
              <Text style={styles.textContainer}>
                <Text style={styles.sectionTitle}>Class & Race Details</Text>
              </Text>
              <Text style={styles.textContainer}>
                <Text style={styles.infoText}>Cantrips:</Text>{" "}
                <Text style={styles.text}>
                  {myClass.cantrips > 0
                    ? `Pick ${myClass.cantrips} ${myClass.name} cantrips`
                    : 0}
                </Text>
              </Text>
              <Text style={styles.textContainer}>
                <Text style={styles.infoText}>Spells:</Text>{" "}
                <Text style={styles.text}>
                  {myRace.spells.length && myClass.spells > 0
                    ? `${myRace.spells}`
                    : `Pick ${myClass.spells} Level 1 ${myClass.name} spells`}
                </Text>
              </Text>
              <Text style={styles.textContainer}>
                <Text style={styles.infoText}>Saving Throws:</Text>{" "}
                <Text style={styles.text}>{characterStats.saving_throws}</Text>
              </Text>
              <Text style={styles.textContainer}>
                <Text style={styles.infoText}>Skill Proficiencies:</Text>{" "}
                <Text style={styles.text}>
                  {characterStats.skill_proficiencies}
                </Text>
              </Text>
              <Text style={styles.textContainer}>
                <Text style={styles.infoText}>Armor Proficiencies:</Text>{" "}
                <Text style={styles.text}>
                  {characterStats.armor_proficiencies}
                </Text>
              </Text>
              <Text style={styles.textContainer}>
                <Text style={styles.infoText}>Weapon Proficiencies:</Text>{" "}
                <Text style={styles.text}>
                  {characterStats.weapon_proficiencies}
                </Text>
              </Text>
              <Text style={styles.textContainer}>
                <Text style={styles.infoText}>Tool Proficiencies:</Text>{" "}
                <Text style={styles.text}>
                  {characterStats.tool_proficiencies}
                </Text>
              </Text>
              <Text style={styles.textContainer}>
                <Text style={styles.infoText}>Starting Equipment:</Text>{" "}
                <Text style={styles.text}>{characterStats.equipment}</Text>
              </Text>
              <View style={styles.text}>{characterStats.racialFeatures}</View>
            </View>
          </View>
        </ScrollView>
      )}

      {/*//! SUBMISSION/RESET BUTTONS */}
      {loading ? null : (
        <View style={styles.submissionContainer}>
          <TouchableOpacity
            style={styles.resetButton}
            onPress={() => {
              props.logout();
              props.navigation.navigate("Home");
            }}
          >
            <Text style={[styles.btnText, { color: "#9d0208" }]}>
              Start Over?
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.saveButton}
            onPress={() => {
              onShare();
            }}
          >
            <Text style={styles.btnText}>Share</Text>
            <Icon name="share" size={30} color="#fff" />
          </TouchableOpacity>
        </View>
      )}
    </SafeAreaView>
  );
};

export default CharacterSheet;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    width: "100%",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  bottom: {
    flexDirection: "column",
    alignItems: "flex-start",
    flexShrink: 1,
    width: "90%",
    padding: 5,
    // borderWidth: StyleSheet.hairlineWidth,
  },
  btnText: {
    textAlign: "center",
    fontSize: 20,
    color: "#fff",
  },
  characterInfo: {
    marginBottom: 10,
    alignSelf: "stretch",
  },
  divider: {
    marginTop: 10,
    marginBottom: 10,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: "#000",
    alignSelf: "stretch",
  },
  // imageContainer: {
  //   flexDirection: "row",
  //   width: "100%",
  //   marginTop: 20,
  //   alignItems: "center",
  //   justifyContent: "center",
  // image: {
  //   width: 300,
  //   height: 300,
  //   resizeMode: "contain",
  // },
  infoText: {
    color: "#9d0208",
    fontSize: 16,
    lineHeight: 30,
    textAlign: "left",
  },
  loadingContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  loadingText: {
    textAlign: "center",
    color: "#9d0208",
    fontSize: 24,
  },
  main: {
    alignItems: "center",
    justifyContent: "center",
  },
  textContainer: {
    color: "#000",
    marginTop: 5,
  },
  resetButton: {
    width: 150,
    height: 40,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "#9d020850", // IOS
    shadowOffset: { height: 2, width: 2 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
  },
  saveButton: {
    flexDirection: "row",
    width: 150,
    height: 40,
    backgroundColor: "#90be6d",
    borderColor: "#bc6c25",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "space-evenly",
    shadowColor: "rgba(0,0,0, .3)", // IOS
    shadowOffset: { height: 3, width: 3 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    elevation: 2, // Android
  },
  sectionTitle: {
    color: "#f00000",
    fontSize: 20,
  },
  submissionContainer: {
    marginTop: 10,
    marginBottom: 10,
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
  },
  text: {
    fontSize: 14,
    flexShrink: 1,
    // backgroundColor: '#ff44ff88'
  },
  title: {
    fontSize: 26,
    color: "#9d0208",
    textAlign: "center",
  },
});
