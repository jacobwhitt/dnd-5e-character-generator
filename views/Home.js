import React, { useEffect } from "react";
import {
  Alert,
  Dimensions,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  KeyboardAvoidingView,
  Keyboard,
  SafeAreaView,
  StatusBar,
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import {
  TouchableOpacity,
  TouchableWithoutFeedback,
} from "react-native-gesture-handler";
const { width, height } = Dimensions.get("window");

const errorMessage = () => {
  console.log("errorMessage triggered");
  Alert.alert("Please enter a name for your character");
};

const Home = (props) => {
  useEffect(() => props.readStorage, []);

  return (
    <KeyboardAvoidingView behavior="padding">
      <SafeAreaView contentContainerStyle={styles.container}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
          <View style={styles.main}>
            <Image style={styles.logo} source={require("../assets/logo.png")} />
            <Text style={styles.titulo}>JW's D&D 5e</Text>
            <Text style={styles.subtitulo}>Character Generator</Text>

            <View style={styles.bodyText}>
              <Text style={styles.text}>
                Enter a name and click the button below to choose premade basics
                for your new character: Race, class and background. Creating a
                playable D&D 5e character was never so quick and easy!
              </Text>
            </View>

            {/* DIVIDER */}
            <View
              style={{
                width: "80%",
                marginTop: 10,
                marginBottom: 10,
                borderBottomWidth: StyleSheet.hairlineWidth,
                borderBottomColor: "#000",
                alignSelf: "center",
              }}
            />

            {props.loggedIn === true ? (
              <>
                {/* BUTTON TO START SELECTING CHARACTER TRAITS */}
                <TouchableOpacity
                  style={[styles.button, { backgroundColor: "#00b4d8" }]}
                  onPress={() => props.navigation.navigate("Race")}
                >
                  <Text style={styles.btnText}>
                    {props.user.statsModded
                      ? "Wanna Pick New Traits?"
                      : "Start Your Adventure!"}
                  </Text>
                </TouchableOpacity>

                {/* JUMP TO CHARACTER SHEET FILLED VIA ASYNC STORAGE IF A CHARACTER WAS ALREADY FINISHED AND STORED */}
                {props.user.statsModded ? (
                  <TouchableOpacity
                    style={[styles.button, { backgroundColor: "#90be6d" }]}
                    onPress={() => props.navigation.navigate("CharacterSheet")}
                  >
                    <Text style={styles.btnText}>
                      {props.user.name.length > 10
                        ? props.user.name.substring(0, 10) + "..."
                        : props.user.name}
                      's Info
                    </Text>
                    <Icon
                      name="user-circle"
                      size={30}
                      color="#fff"
                      style={{ position: "absolute", left: 30 }}
                    />
                    <Icon
                      name="user-circle"
                      size={30}
                      color="#fff"
                      style={{ position: "absolute", right: 30 }}
                    />
                  </TouchableOpacity>
                ) : null}

                {/* LOG OUT AND CLEAR ASYNC STORAGE */}
                <TouchableOpacity
                  style={styles.button}
                  onPress={() => props.logout()}
                >
                  <Text style={styles.btnText}>Logout</Text>
                </TouchableOpacity>
              </>
            ) : (
              <View style={styles.nameInput}>
                <Text style={styles.text}>Enter your character's name: </Text>
                <View style={styles.input}>
                  <TextInput
                    placeholder="Character name"
                    value={props.name}
                    onChangeText={(text) => props.setName(text)}
                    style={styles.inputText}
                  />
                </View>

                <TouchableOpacity
                  style={styles.button}
                  onPress={() => {
                    return !props.name
                      ? errorMessage()
                      : (props.login(), props.navigation.navigate("Race"));
                  }}
                >
                  <Text style={styles.btnText}>Build Your Character!</Text>
                </TouchableOpacity>
              </View>
            )}

            {/*//! END OF MAIN VIEW STYLE */}
          </View>
        </TouchableWithoutFeedback>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#fff",
    width: width,
    height: height,
  },
  bodyText: {
    width: "95%",
    alignItems: "center",
    justifyContent: "center",
  },
  button: {
    flexDirection: "row",
    marginTop: 20,
    width: width - 20,
    height: 40,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#9d0208",
    shadowColor: "rgba(0,0,0, .3)", // IOS
    shadowOffset: { height: 3, width: 3 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    elevation: 2, // Android
    zIndex: 3,
  },
  btnText: {
    color: "white",
    fontSize: 24,
  },
  input: {
    height: 50,
    width: 200,
    borderWidth: 1,
    borderColor: "#ccc",
    alignItems: "center",
    justifyContent: "center",
  },
  inputText: {
    width: "100%",
    height: "100%",
    fontSize: 24,
    textAlign: "center",
  },
  logo: {
    height: 70,
    marginBottom: 10,
    resizeMode: "contain",
    alignSelf: "center",
  },
  main: {
    width: width,
    height: height,
    alignItems: "center",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  nameInput: {
    alignItems: "center",
  },
  text: {
    color: "#9d0208",
    textAlign: "center",
    lineHeight: 40,
    fontSize: 24,
  },
  titulo: {
    color: "#f00000",
    fontSize: 30,
    fontWeight: "bold",
  },
  subtext: {
    textAlign: "center",
    lineHeight: 24,
    fontSize: 16,
    width: "70%",
  },
  subtitulo: {
    marginTop: 10,
    color: "#f00000",
    fontSize: 30,
    fontWeight: "bold",
  },
});
