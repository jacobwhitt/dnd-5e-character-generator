import React, { useState, useEffect } from "react";
import * as ImagePicker from "expo-image-picker";
import { SafeAreaView, Dimensions, StyleSheet, Text, View, Image } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import BackBtn from "../components/backBtn";

const Photo = (props) => {
  const [image, setImage] = useState(null);

  useEffect(() => {
    const getPermission = async () => {
      if (Platform.OS !== "web") {
        const { status } = await ImagePicker.requestCameraRollPermissionsAsync();
        if (status !== "granted") {
          alert("Sorry, we need camera roll permissions to make this work!");
        }
      }
    };
  }, []);

  const uploadPhoto = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [16, 9],
      quality: 1,
    });

    if (!result.cancelled) {
      setImage(result.uri);
      props.saveUploadedPhoto(result.uri);
    }

    console.log("Image uploaded successfully: ", result);
  };

  return (
    <SafeAreaView style={styles.container}>
      <BackBtn text='Background' onPress='Background' color='#90be6d'></BackBtn>
      <View style={styles.main}>
        <Text style={styles.titulo}>Add a Photo of your Character:</Text>

        <TouchableOpacity style={styles.button} onPress={uploadPhoto}>
          <Text style={styles.btnText}>Upload Image</Text>
        </TouchableOpacity>

        {image && <Image source={{ uri: image }} style={styles.image} />}

        {image && (
          <TouchableOpacity
            style={[styles.button]}
            onPress={() => {
              props.navigation.navigate("CharacterSheet");
            }}>
            <Text style={styles.btnText}>Next</Text>
          </TouchableOpacity>
        )}
      </View>
    </SafeAreaView>
  );
};

export default Photo;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center",
  },
  main: {
    marginTop: 40,
    alignItems: "center",
    justifyContent: "center",
  },
  titulo: {
    color: "#90be6d",
    fontSize: 30,
  },
  image: {
    marginTop: 20,
    width: 300,
    height: 300,
    resizeMode: "contain",
    alignSelf: "center",
  },
  imageContainer: {
    width: 320,
    height: 300,
    margin: 200,
    alignItems: "center",
    justifyContent: "center",
  },
  caption: {
    marginTop: 20,
    textAlign: "center",
    fontSize: 20,
  },
  text: {
    textAlign: "center",
    fontSize: 20,
    color: "white",
  },
  button: {
    marginTop: 20,
    width: 120,
    height: 60,
    backgroundColor: "#90be6d",
    borderColor: "#bc6c25",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "rgba(0,0,0, .3)", // IOS
    shadowOffset: { height: 3, width: 3 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    elevation: 2, // Android
  },
  btnText: {
    fontSize: 20,
    color: "#fff",
  },
});
