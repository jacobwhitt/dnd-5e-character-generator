import React from "react";
import {
  Dimensions,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  StatusBar,
} from "react-native";
import { FlatList, TouchableOpacity } from "react-native-gesture-handler";
import { Races } from "../components/races";
import BackBtn from "../components/backBtn";
// import

const { width, height } = Dimensions.get("window");
let large;
if (width > 500) {
  large = 300;
} else {
  large = 120;
}

const Race = (props) => {
  const razas = props.raceNames;

  selectRace = (race) => {
    console.log("race button pushed: ", race);
    const index = (element) => element.name === race;
    let raceIndex = Races.findIndex(index);
    props.setRace(raceIndex);
  };

  return (
    <SafeAreaView style={styles.container}>
      <BackBtn text="Home" onPress="Home" color="#9d0208"></BackBtn>
      <View style={styles.main}>
        <Text style={styles.titulo}>Choose a Race:</Text>

        <FlatList
          numColumns={2}
          data={razas}
          renderItem={({ item }) => (
            <View style={styles.gridWrapper} key={item.id}>
              <TouchableOpacity
                style={styles.button}
                onPress={() => {
                  selectRace(`${item.name}`);
                  props.navigation.navigate("Class");
                }}
              >
                <Text style={styles.text}>{item.name}</Text>
              </TouchableOpacity>
            </View>
          )}
        />
      </View>
    </SafeAreaView>
  );
};

export default Race;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  button: {
    width: large,
    height: 40,
    backgroundColor: "crimson",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "rgba(0,0,0, .3)", // IOS
    shadowOffset: { height: 3, width: 3 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    elevation: 2, // Android
  },
  gridWrapper: {
    height: 40,
    margin: 15,
    alignItems: "center",
    justifyContent: "center",
  },
  main: {
    marginTop: 30,
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    fontSize: 18,
    color: "white",
  },
  titulo: {
    color: "crimson",
    fontSize: 30,
  },
  subtitulo: {
    color: "brown",
    fontWeight: "bold",
    fontSize: 20,
    marginTop: 10,
  },
});
